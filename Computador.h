//
//  Computador.h
//  TBlackjack
//
//  Created by Vinicius Tonon on 10/8/13.
//  Copyright (c) 2013 Vinicius Tonon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dealer.h"

@interface Computador : NSObject

@property (nonatomic) BOOL dobrar;
@property (nonatomic) BOOL jogar;
@property(nonatomic, weak) Dealer *dealer;

-(BOOL)ComputadorJogar;

@end
