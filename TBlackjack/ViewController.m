//
//  ViewController.m
//  TBlackjack
//
//  Created by Vinicius Tonon on 10/8/13.
//  Copyright (c) 2013 Vinicius Tonon. All rights reserved.
//

#import "ViewController.h"
#import "Dealer.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *textoDinheiro;
@property (weak, nonatomic) IBOutlet UILabel *textAposta;
@property (weak, nonatomic) IBOutlet UILabel *textCartas;
@property(nonatomic, strong) Dealer *dealer;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.dealer = [[Dealer alloc] init];
        [self updateTextos];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonPlay:(id)sender {
    [self.dealer Jogar];
    self.dealer.jogadorDeal = TRUE;
        [self updateTextos];
    if(self.dealer.doubleOrNothing)
    {
        [self alertDouble];
        self.dealer.doubleOrNothing = NO;
    }
}
- (IBAction)buttonStop:(id)sender {
    BOOL parada = [self.dealer Parar];
        [self updateTextos];
    if(parada){
        [self alertGanhou];
    }else{
        [self alertPerdeu];
    }
}
- (IBAction)buttonPararDrag:(id)sender {
    
}
- (IBAction)longPressParar:(id)sender {
    self.dealer.cartasJogador = nil;
    self.dealer.cartasComputador = nil;
    self.dealer.aposta = 0;
    self.dealer.computadorSum = 0;
    self.dealer.jogadorSum = 0;
        [self updateTextos];
}

-(void)alertDouble
{
    UIAlertView *vAlertDouble = [[UIAlertView alloc] initWithTitle:@"Dobro ou nada" message:@"O dealer pediu para você apostar o dobro ou correr" delegate:self cancelButtonTitle:@"Aceitar" otherButtonTitles:@"Correr", nil];
    [vAlertDouble show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"%d, ALERT DOUBLE", buttonIndex);
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    NSLog(@"%@", title);
    if(buttonIndex == 0)
    {
        if(self.dealer.jogadorSum == 21 && self.dealer.jogadorSum == 20){
            [self.dealer Parar];
        } else {
            [self.dealer Jogar];
            self.dealer.jogadorDeal = TRUE;
        [self updateTextos];
            self.dealer.dinheiro -= self.dealer.aposta;
            self.dealer.aposta += self.dealer.aposta*2;
        }
    }else if(buttonIndex == 1)
    {
        NSLog(@"Correr");
        BOOL parada = [self.dealer Parar];
        [self updateTextos];
        if(parada){
            [self alertGanhou];
        }else{
            [self alertPerdeu];
        }
    }
}

-(void)alertPerdeu
{
    UIAlertView *vAlertPerdeu = [[UIAlertView alloc] initWithTitle:@"Perdeu" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [vAlertPerdeu show];
}

-(void)alertGanhou
{
    UIAlertView *vAlertGanhou = [[UIAlertView alloc]initWithTitle:@"Ganhou" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [vAlertGanhou show];
}
-(void)updateTextos
{
    [self.textoDinheiro setText:[NSString stringWithFormat:@"Dinheiro: %d", self.dealer.dinheiro]];
    [self.textAposta setText:[NSString stringWithFormat:@"Aposta: %d", self.dealer.aposta]];
    [self.textCartas setText:[NSString stringWithFormat:@"Cartas: %@ Soma das Cartas: %d",[NSString stringWithFormat:@"%@", [self.dealer.cartasJogador componentsJoinedByString:@""]], self.dealer.jogadorSum]];
}
@end
