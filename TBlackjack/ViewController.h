//
//  ViewController.h
//  TBlackjack
//
//  Created by Vinicius Tonon on 10/8/13.
//  Copyright (c) 2013 Vinicius Tonon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIAlertViewDelegate>

-(void)alertDouble;
-(void)alertGanhou;
-(void)alertPerdeu;

@end
