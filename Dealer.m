//
//  Dealer.m
//  TBlackjack
//
//  Created by Vinicius Tonon on 10/8/13.
//  Copyright (c) 2013 Vinicius Tonon. All rights reserved.
//

#import "Dealer.h"
#import "Computador.h"
#import "ViewController.h"

@interface Dealer()
@property(nonatomic, strong) Computador *computador;
@end

@implementation Dealer

-(id)init
{
    self = [super init];
    if(self){
        self.cartasComputador = [[NSMutableArray alloc] init];
        self.cartasJogador = [[NSMutableArray alloc] init];
        self.computadorDeal = TRUE;
        self.jogadorDeal = TRUE;
        self.computador = [[Computador alloc] init];
        self.computador.dealer = self;
        self.aposta = 0;
        self.dinheiro = 20;        
    }
    return self;
}

-(int)Randomizer
{
    int random;
    random = arc4random() % 12;
    return random;
}

-(void)Deal
{
    if(self.jogadorDeal)
    {
        int carta = [self Randomizer];
        [self.cartasJogador addObject:[NSString stringWithFormat:@"%u, ", carta+1]];
        self.jogadorSum = self.jogadorSum + (carta+1);
        self.jogadorDeal = FALSE;
    }
    if(self.computador.ComputadorJogar)
    {
        int carta = [self Randomizer];
        [self.cartasComputador addObject:[NSString stringWithFormat:@"%u, ", carta+1]];
        self.computadorSum = self.computadorSum + (carta+1);
        self.computadorDeal = FALSE;
        NSLog(@"%d", self.computadorSum);
    }
}

-(void)Jogar
{
    if(self.aposta == 0){
        self.aposta += 5; self.dinheiro -= 5;}
    [self Deal];
}
-(BOOL)Parar
{
    BOOL parada;
    if(self.jogadorSum > 21)
    {
        NSLog(@"Perdeu");
        parada = NO;
    }
    if(self.jogadorSum == 21 && self.computadorSum != 21){
        self.dinheiro += self.aposta*2;
        parada = YES;
    }
    if(self.computadorSum > 21)
    {
        if(self.computadorSum < self.jogadorSum)
        {
            parada = NO;
        } else {
        NSLog(@"Ganhou");
        self.dinheiro += self.aposta*2;
        parada = YES;
        }
    }
    if(self.jogadorSum > self.computadorSum && self.jogadorSum <= 21)
    {
        self.dinheiro += self.aposta*2;
        parada = YES;
    }
    if(self.computadorSum > self.jogadorSum && self.computadorSum <= 21){
        NSLog(@"Perdeu");
        parada = NO;
    }
    if(self.jogadorSum == self.computadorSum)
    {
        parada = NO;
        NSLog(@"Perdeu");
    }
    if(self.computadorSum == self.jogadorSum)
    {
        parada = NO;
        NSLog(@"Perdeu");
    }
    self.aposta = 0;
    self.jogadorSum = 0;
    self.computadorSum = 0;
    [self.cartasComputador removeAllObjects];
    [self.cartasJogador removeAllObjects];
    
    return parada;
}
-(void)DoubleOrNothing
{
    self.doubleOrNothing = TRUE;
}

@end
