//
//  Dealer.h
//  TBlackjack
//
//  Created by Vinicius Tonon on 10/8/13.
//  Copyright (c) 2013 Vinicius Tonon. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dealer : NSObject

@property(nonatomic, strong) NSMutableArray *cartasJogador;
@property(nonatomic, strong) NSMutableArray *cartasComputador;
@property(nonatomic) int jogadorSum;
@property(nonatomic) int computadorSum;
@property(nonatomic) int aposta;
@property(nonatomic) int dinheiro;
@property(nonatomic) BOOL playing;
@property(nonatomic) BOOL jogadorDeal;
@property(nonatomic) BOOL computadorDeal;
@property(nonatomic) BOOL doubleOrNothing;
@property(nonatomic) NSNumber *rando;

-(void)Deal;

-(void)Jogar;

-(BOOL)Parar;

-(void)DoubleOrNothing;


@end
