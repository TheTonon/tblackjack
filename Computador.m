//
//  Computador.m
//  TBlackjack
//
//  Created by Vinicius Tonon on 10/8/13.
//  Copyright (c) 2013 Vinicius Tonon. All rights reserved.
//

#import "Computador.h"
#import "Dealer.h"
@interface Computador()
@end

@implementation Computador


-(id)init
{
    self = [super init];
    if(!self){
    }
    return self;
}


-(BOOL)ComputadorJogar
{
    if(self.dealer.computadorSum < 9)
    {
        return TRUE;
        NSLog(@"DEAL <9");
    }
    if((self.dealer.computadorSum) < 12 && (self.dealer.computadorSum >= 9))
    {
        if(self.dealer.aposta < 6)
        {
            NSLog(@"DEAL <12 aposta 6 Double ON");
            self.dealer.doubleOrNothing = YES;
            return TRUE;
        }
    }
    if(self.dealer.computadorSum < 15 && self.dealer.computadorDeal >= 12)
    {
        NSLog(@"DEAL <15 Double ON");
        self.dealer.doubleOrNothing = YES;
        return TRUE;
    }
    if(self.dealer.computadorSum == 21)
    {
        NSLog(@"DEAL == 21 Double ON");
        self.dealer.doubleOrNothing = YES;
        return TRUE;
    }
    if(self.dealer.computadorSum > 17)
    {
        return NO;
    }
    self.dealer.doubleOrNothing = YES;
    return 0;
}

@end
